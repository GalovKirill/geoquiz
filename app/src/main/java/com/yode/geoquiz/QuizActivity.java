package com.yode.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;

public class QuizActivity extends AppCompatActivity {

    private static final String COUNT_CHEAT_USED = "count cheat used";
    private static final String IS_CHEATER = "isCheater";

    private Revolver<Question> mQuestionBank = new Revolver<>(
            new Question[]{
                new Question(R.string.question_africa, false),
                new Question(R.string.question_oceans, true),
                new Question(R.string.question_mideast, false),
                new Question(R.string.question_americas, true),
                new Question(R.string.question_asia, true)
    });

    private boolean[] mIsCheaters = new boolean[]{false,false,false,false,false};
    private int mCheaterCurrentIndex;

    private static final String TAG = "QuizActivity";
    private static final String KEY_INDEX = "index";

    private Button mTrueButton;
    private Button mFalseButton;
    private TextView mQuestionTextView;
    private Button mNextButton;
    private Button mCheatButton;
    private final int REQUEST_CODE_CHEAT = 0;

    int mCountCHeatUsed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate called");
        setContentView(R.layout.activity_quiz);

        if (savedInstanceState != null){
            mQuestionBank.setCurrentIndex(savedInstanceState.getInt(KEY_INDEX));
            mIsCheaters = savedInstanceState.getBooleanArray(IS_CHEATER);
        }

        mTrueButton = findViewById(R.id.true_button);
        mFalseButton = findViewById(R.id.false_button);
        mNextButton = findViewById(R.id.next_button);
        mCheatButton = findViewById(R.id.cheat_button);
        mQuestionTextView = findViewById(R.id.question_text_view);

        mQuestionTextView.setText(mQuestionBank.getCurrent().getTextResId());

        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCountCHeatUsed >=3){
                    mCheatButton.setEnabled(false);
                    return;
                }
                mCountCHeatUsed++;
                boolean answerIsTrue = mQuestionBank.getCurrent().isAnswerTrue();
                Intent intent = CheatActivity.newIntent(QuizActivity.this, answerIsTrue);
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
                mTrueButton.setEnabled(false);
                mFalseButton.setEnabled(false);
            }
        });

        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
                mFalseButton.setEnabled(false);
                mTrueButton.setEnabled(false);
            }
        });


        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateQuestion();
                mCheaterCurrentIndex = (mCheaterCurrentIndex + 1) % mIsCheaters.length;
                mFalseButton.setEnabled(true);
                mTrueButton.setEnabled(true);
            }
        });

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState");
        outState.putInt(KEY_INDEX, mQuestionBank.getCurrentIndex());
        outState.putBooleanArray(IS_CHEATER, mIsCheaters);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onCreate called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_CODE_CHEAT){
            if(data == null) return;
            mIsCheaters[mCheaterCurrentIndex] = CheatActivity.wasAnswerShow(data);
        }

    }

    private void updateQuestion(){
        Question question = mQuestionBank.getNext();
        mQuestionTextView.setText(question.getTextResId());
    }



    private void checkAnswer(boolean userPressedTrue){
        Question question = mQuestionBank.getCurrent();
        int messageResId = mIsCheaters[mCheaterCurrentIndex] ? R.string.judgment_toast :
                question.isAnswerTrue() == userPressedTrue ? R.string.correct_toast : R.string.incorrect_toast;
        Toast toast = Toast.makeText(this, messageResId, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onTextViewClick(View view) {
        updateQuestion();
    }
}
