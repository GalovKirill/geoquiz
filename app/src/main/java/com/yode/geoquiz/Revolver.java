package com.yode.geoquiz;

public class Revolver<T> {

    public Revolver(T[] mElements) {
        this.mElements = mElements;
    }

    public T[] getElements() {
        return mElements;
    }

    public void setElements(T[] mElements) {
        this.mElements = mElements;
    }

    private T[] mElements;
    private int mCurrentIndex;

    public void setCurrentIndex(int mCurrentIndex) {
        this.mCurrentIndex = mCurrentIndex;
    }

    public int getCurrentIndex() {
        return mCurrentIndex;
    }

    public T getCurrent(){
        return mElements[mCurrentIndex];
    }

    public T getNext(){
        mCurrentIndex = (++mCurrentIndex) % mElements.length;
        return getCurrent();
    }

}
